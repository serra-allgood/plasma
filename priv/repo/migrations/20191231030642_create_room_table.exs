defmodule PlasmaRepo.Repo.Migrations.CreateRoomTable do
  use Ecto.Migration

  def change do
    create table(:rooms) do
      add :mx_room_id, :string, null: false
      add :is_public, :boolean
      timestamps()
    end
  end
end
