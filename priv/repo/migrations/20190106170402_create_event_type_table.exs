defmodule Plasma.Repo.Migrations.CreateEventTypeTable do
  use Ecto.Migration

  def change do
    create table(:event_types) do
      add :name, :string, null: false
      timestamps(updated_at: false)
    end

    create unique_index(:event_types, [:name])
  end
end
