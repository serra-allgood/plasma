defmodule PlasmaRepo.Repo.Migrations.CreateUserTable do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :mx_user_id, :string, null: false
      add :password_hash, :string
      add :kind, :string
      timestamps()
    end

    create unique_index(:users, [:mx_user_id])

  end
end
