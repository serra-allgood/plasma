defmodule Plasma.Repo.Migrations.CreateEventTable do
  use Ecto.Migration

  def change do
    create table(:events) do
      add :event_id, :string
      add :state_key, :string
      add :sender, :string, null: false
      add :prev_event_id, :string
      add :channel_id, references(:channels)
      add :event_type_id, references(:event_types)
      timestamps()
    end
  end
end
