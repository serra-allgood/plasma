defmodule Plasma.Repo.Migrations.AddSequenceNumber do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add :sequence_number, :integer, null: false
    end

    alter table(:snapshots) do
      add :event_sequence_number, :integer, null: false
    end

    alter table(:channels) do
      add :next_event_sequence, :integer, null: false
    end
  end
end
