defmodule Plasma.Repo.Migrations.CreateSnapshotTable do
  use Ecto.Migration

  def change do
    create table(:snapshots) do
      add :snapshot_state, :map
      add :channel_fk, references(:channels)
    end
  end
end
