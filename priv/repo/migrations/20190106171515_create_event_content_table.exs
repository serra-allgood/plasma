defmodule Plasma.Repo.Migrations.CreateEventContentTable do
  use Ecto.Migration

  def change do
    create table(:event_contents) do
      add :content_type, :string
      add :string_content, :string
      add :json_content, :map
      add :compressed_binary_content, :binary
      add :event_id, references(:events)
      timestamps()
    end
  end
end
