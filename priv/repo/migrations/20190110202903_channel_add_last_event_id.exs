defmodule Plasma.Repo.Migrations.ChannelAddLastEventId do
  use Ecto.Migration

  def change do
    alter table(:channels) do
      add :last_event_id, :string
    end
  end
end
