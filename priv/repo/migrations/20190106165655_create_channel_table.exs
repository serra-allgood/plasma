defmodule Plasma.Repo.Migrations.CreateChannelTable do
  use Ecto.Migration

  def change do
    create table(:channels) do
      add :channel_id, :string, null: false
    end

    create unique_index(:channels, [:channel_id])
  end
end
