defmodule PlasmaWeb.Plugs.InteractiveAuthFlowTest do
  use PlasmaWeb.ConnCase
  use Plug.Test

  def clear_cache(_context) do
    PlasmaWeb.Plugs.AuthSessionCache.flush()
    PlasmaRepo.Users.AccessTokenCache.flush()
    PlasmaRepo.Events.EventTypeCache.flush()
    :ok
  end

  setup :clear_cache

  describe "InteractiveAuthFlow" do
    test "return new auth flow response" do
      conn =
        build_conn()
        |> put_req_header("content-type", "application/json")
        |> put_req_header("user-agent", "TEST")
        |> post("/_matrix/client/r0/register")

      body = json_response(conn, 401)
      assert String.length(String.trim(body["session"])) > 0
    end

    test "return new auth flow response for unknown session" do
      conn =
        build_conn()
        |> put_req_header("content-type", "application/json")
        |> put_req_header("user-agent", "TEST")
        |> post("/_matrix/client/r0/register", %{"auth" => %{"session" => "TEST_SESSION"}})

      body = json_response(conn, 401)
      assert String.length(String.trim(body["session"])) > 0
      assert body["session"] != "TEST_SESSION"
    end

    test "return same auth flow response for existing session" do
      conn =
        build_conn()
        |> put_req_header("content-type", "application/json")
        |> put_req_header("user-agent", "TEST")
        |> post("/_matrix/client/r0/register")

      %{"session" => session} = json_response(conn, 401)

      conn =
        build_conn()
        |> put_req_header("content-type", "application/json")
        |> put_req_header("user-agent", "TEST")
        |> post("/_matrix/client/r0/register", %{"auth" => %{"session" => session}})

      body = json_response(conn, 401)
      assert String.length(String.trim(body["session"])) > 0
      assert body["session"] == session
    end

    test "complete auth flow with m.login.dummy" do
      conn =
        build_conn()
        |> put_req_header("content-type", "application/json")
        |> put_req_header("user-agent", "TEST")
        |> post("/_matrix/client/r0/register")

      %{"session" => session} = json_response(conn, 401)

      conn =
        build_conn()
        |> put_req_header("content-type", "application/json")
        |> put_req_header("user-agent", "TEST")
        |> post("/_matrix/client/r0/register", %{
          "auth" => %{"session" => session, "type" => "m.login.dummy"}
        })

      json_response(conn, 200)
    end
  end
end
