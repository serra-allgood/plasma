defmodule PlasmaWeb.Controllers.MatrixApi.Client.VersionsTest do
  use PlasmaWeb.ConnCase

  test "get/2 returns current supported versions", %{conn: conn} do
    response =
      conn
      |> get(Routes.versions_path(conn, :get))
      |> json_response(200)

    assert response == %{"versions" => ["r0.4.0", "r0.5.0", "r0.6.0"]}
  end
end
