defmodule PlasmaWeb.Controllers.MatrixApi.Client.AccountTest do
  use PlasmaWeb.ConnCase

  def clear_cache(_context) do
    PlasmaWeb.Plugs.AuthSessionCache.flush()
    PlasmaRepo.Users.AccessTokenCache.flush()
    PlasmaRepo.Events.EventTypeCache.flush()
    :ok
  end

  setup :clear_cache

  test "whoami returns current user_id", %{conn: conn} do
    # Register a new user
    request = %{
      "auth" => %{
        "session" => get_auth_session(conn),
        "type" => "m.login.dummy"
      }
    }

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> post(Routes.register_path(conn, :register), request)
      |> json_response(200)

    assert %{"access_token" => access_token, "device_id" => device_id, "user_id" => user_id} =
             response
    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> put_req_header("authorization", "Bearer " <> access_token)
      |> post(Routes.account_path(conn, :whoami))
      |> json_response(200)

    assert %{"user_id" => test_user_id} = response
    assert test_user_id == user_id
  end

  test "whoami returns when M_MISSING_TOKEN", %{conn: conn} do
    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> put_req_header("authorization", "Bearer " <> "dummy")
      |> post(Routes.account_path(conn, :whoami))
      |> json_response(401)

    assert %{"errcode" => errcode} = response
    assert errcode == "M_UNKNOWN_TOKEN"
  end
end
