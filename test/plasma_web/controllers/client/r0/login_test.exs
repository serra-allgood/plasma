defmodule PlasmaWeb.Controllers.MatrixApi.Client.LoginTest do
  use PlasmaWeb.ConnCase
  import PlasmaRepo.Factory

  def clear_cache(_context) do
    PlasmaWeb.Plugs.AuthSessionCache.flush()
    PlasmaRepo.Users.AccessTokenCache.flush()
    PlasmaRepo.Events.EventTypeCache.flush()
    :ok
  end

  setup :clear_cache

  test "login with invalid login type returns M_UKNOWN error", %{conn: conn} do
    request = %{
      "type" => "some.type",
      "identifier" => %{
        "type" => "m.id.user",
        "user" => "cheeky_monkey"
      },
      "password" => "ilovebananas",
      "initial_device_display_name" => "Jungle Phone"
    }

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> post(Routes.login_path(conn, :login), request)
      |> json_response(400)

    assert %{"errcode" => errcode} = response
    assert errcode == "M_UNKNOWN"
  end

  test "login with m.login.password returns M_UKNOWN error for invalid identifier type", %{
    conn: conn
  } do
    request = %{
      "type" => "m.login.password",
      "identifier" => %{
        "type" => "some.type",
        "user" => "cheeky_monkey"
      },
      "password" => "ilovebananas",
      "initial_device_display_name" => "Jungle Phone"
    }

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> post(Routes.login_path(conn, :login), request)
      |> json_response(400)

    assert %{"errcode" => errcode} = response
    assert errcode == "M_UNKNOWN"
  end

  test "login with m.login.password succeed M_FORBIDDEN for unknown user", %{conn: conn} do
    request = %{
      "type" => "m.login.password",
      "identifier" => %{
        "type" => "m.id.user",
        "user" => "cheeky_monkey"
      },
      "password" => "ilovebananas",
      "initial_device_display_name" => "Jungle Phone"
    }

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> post(Routes.login_path(conn, :login), request)
      |> json_response(403)

    assert %{"errcode" => errcode} = response
    assert errcode == "M_FORBIDDEN"
  end

  test "login with m.login.password succeed with user id and existing device", %{conn: conn} do
    some_user = insert(:user, %{password: "PASSWORD"})

    request = %{
      "type" => "m.login.password",
      "identifier" => %{
        "type" => "m.id.user",
        "user" => to_string(some_user.mx_user_id)
      },
      "password" => "PASSWORD",
      "device_id" => "DEVICE"
    }

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> post(Routes.login_path(conn, :login), request)
      |> json_response(200)

    assert %{"access_token" => token, "user_id" => user_id, "device_id" => device_id} = response
    assert user_id == some_user.mx_user_id
    assert device_id == "DEVICE"
  end

  test "login with m.login.password succeed with user id and no device", %{conn: conn} do
    some_user = insert(:user)

    request = %{
      "type" => "m.login.password",
      "identifier" => %{
        "type" => "m.id.user",
        "user" => to_string(some_user.mx_user_id)
      },
      "password" => "PASSWORD",
      "initial_display_name" => "test_case"
    }

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> post(Routes.login_path(conn, :login), request)
      |> json_response(200)

    assert %{"access_token" => token, "user_id" => user_id, "device_id" => device_id} = response
    assert user_id == to_string(some_user.mx_user_id)
  end

  test "login with m.login.password fails with wrong password", %{conn: conn} do
    some_user = insert(:user)

    request = %{
      "type" => "m.login.password",
      "identifier" => %{
        "type" => "m.id.user",
        "user" => some_user.mx_user_id
      },
      "password" => "HACK_PASSWORD",
      "device_id" => "DEVICE"
    }

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> post(Routes.login_path(conn, :login), request)
      |> json_response(403)

    assert %{"errcode" => errcode} = response
    assert errcode == "M_FORBIDDEN"
  end

  test "logout with unknown token fail with M_MISSING_TOKEN", %{conn: conn} do
    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> post(Routes.login_path(conn, :logout))
      |> json_response(401)

    assert %{"errcode" => errcode} = response
    assert errcode == "M_MISSING_TOKEN"
  end

  test "logout succeed with token from existing user", %{conn: conn} do
    # Register a new user
    request = %{
      "auth" => %{
        "session" => get_auth_session(conn),
        "type" => "m.login.dummy"
      }
    }

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> post(Routes.register_path(conn, :register), request)
      |> json_response(200)

    assert %{"access_token" => access_token, "device_id" => device_id, "user_id" => user_id} =
             response

    user = PlasmaRepo.Users.get_by_mx_user_id_with_devices(user_id)
    assert user != nil
    assert length(user.devices) > 0

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> put_req_header("authorization", "Bearer " <> access_token)
      |> post(Routes.login_path(conn, :logout))
      |> json_response(200)

    assert %{} = response
    user = PlasmaRepo.Users.get_by_mx_user_id_with_devices(user_id)
    assert user != nil
    assert length(user.devices) == 0
  end

  test "logout_all with unknown token fail with M_UNKNOWN_TOKEN", %{conn: conn} do
    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> put_req_header("authorization", "Bearer " <> "dummy")
      |> post(Routes.login_path(conn, :logout_all))
      |> json_response(401)

    assert %{"errcode" => errcode} = response
    assert errcode == "M_UNKNOWN_TOKEN"
  end

  test "logout_all succeed with token from existing user", %{conn: conn} do
    # Register a new user
    request = %{
      "auth" => %{
        "session" => get_auth_session(conn),
        "type" => "m.login.dummy"
      }
    }

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> post(Routes.register_path(conn, :register), request)
      |> json_response(200)

    assert %{"access_token" => access_token, "device_id" => device_id, "user_id" => user_id} =
             response

    user = PlasmaRepo.Users.get_by_mx_user_id_with_devices(user_id)
    assert user != nil
    assert user.devices != []

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> put_req_header("authorization", "Bearer " <> access_token)
      |> post(Routes.login_path(conn, :logout_all))
      |> json_response(200)

    assert %{} = response
    user = PlasmaRepo.Users.get_by_mx_user_id_with_devices(user_id)
    assert user != nil
    assert user.devices == []
  end
end
