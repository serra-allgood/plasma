defmodule PlasmaWeb.Plugs.ExtractAccessTokenTest do
  use PlasmaWeb.ConnCase

  alias PlasmaWeb.Plugs.ExtractAccessToken

  @opts ExtractAccessToken.init([])

  describe "ProvideAccessToken" do
    test "test assign token from query string", %{conn: conn} do
      conn = conn |> get("/_matrix/client/r0/login?access_token=TOKEN")
        |> ExtractAccessToken.call(@opts)

      assert conn.assigns[:access_token] == "TOKEN"
    end

    test "test assign token from request header" do
      conn =
        build_conn()
        |> put_req_header("authorization", "Bearer TOKEN")
        |> ExtractAccessToken.call(@opts)

      assert conn.assigns[:access_token] == "TOKEN"
    end

    test "test don't assign token with malformed request header" do
      conn =
        build_conn()
        |> put_req_header("authorization", "TOKEN")
        |> ExtractAccessToken.call(@opts)

      assert conn.assigns[:access_token] == nil
    end

    test "test don't assign token when no token is provided" do
      conn = build_conn() |> ExtractAccessToken.call(@opts)
      assert conn.assigns[:access_token] == nil
    end
  end
end
