defmodule PlasmaRepo.Factory do
  use ExMachina.Ecto, repo: PlasmaRepo.Repo

  def event_type_factory() do
    %PlasmaRepo.Events.EventType{name: "event.type.test"}
  end

  def event_factory() do
    %PlasmaRepo.Events.Event{
      event_id: sequence("event"),
      sequence_number: 1,
      state_key: sequence("state"),
      sender: sequence("sender"),
      event_type: build(:event_type),
      channel: build(:channel)
    }
  end

  def build_channel() do
    %PlasmaRepo.Channels.Channel{
      channel_id: sequence("channel")
    }
  end

  def user_factory() do
    %PlasmaRepo.Users.User {
      mx_user_id: sequence(:mx_user_id, &"@user#{&1}:localhost"),
      password: "PASSWORD",
      password_hash: Argon2.hash_pwd_salt("PASSWORD")
    }
  end

  def device_factory() do
    %PlasmaRepo.Users.Device {
      mx_device_id: sequence("DEVICE"),
      user: build(:user)
    }
  end

  def room_factory() do
    %PlasmaRepo.Rooms.Room {
      mx_room_id: sequence(:mx_room_id, &"!room#{&1}:localhost"),
    }
  end

end
