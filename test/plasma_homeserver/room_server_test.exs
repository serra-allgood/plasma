defmodule PlasmaHS.RoomServerTests do
  use PlasmaRepo.DataCase
  import PlasmaRepo.Factory
  alias PlasmaHS.RoomServer

  def clear_cache(_context) do
    PlasmaRepo.Events.EventTypeCache.flush()
    :ok
  end

  setup :clear_cache

  test "start room server on existing room succeed" do
    room = insert(:room)
    res = RoomServer.start_room_server(room.id)
    assert {:ok, _pid} = res
  end

  test "start room server on non-existing fails" do
    res = RoomServer.start_room_server(Ecto.UUID.generate())
    assert {:error, _pid} = res
  end

end