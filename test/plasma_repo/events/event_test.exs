defmodule PlasmaRepo.Events.EventTest do
  use PlasmaRepo.DataCase
  alias PlasmaRepo.Events.EventType
  alias PlasmaRepo.Channels
  alias PlasmaRepo.Events.Event

  def clear_cache(_context) do
    PlasmaRepo.Events.EventTypeCache.flush()
    :ok
  end

  setup :clear_cache

  describe "Event" do
    test "create_event/1 should succeed creating an event with existing event_type" do
      with {:ok, event_type} <- EventType.get_or_create("test.type"),
           {:ok, channel} <- Channels.create_channel(%{channel_id: "channel"}) do
        event =
          Event.create_event(%{
            sequence_number: 1,
            state_key: "state",
            sender: "sender",
            type_name: event_type.name,
            channel_id: channel.id
          })

        assert {:ok, _} = event
      end
    end

    test "create_event/1 should succeed creating an event with type_name" do
      with {:ok, _event_type} <- EventType.get_or_create("test.type.2"),
           {:ok, channel} <- Channels.create_channel(%{channel_id: "channel"}) do
        creation =
          Event.create_event(%{
            sequence_number: 1,
            state_key: "state",
            sender: "sender",
            type_name: "test.type.2",
            channel_id: channel.id
          })

        assert {:ok, event} = creation
      end
    end
  end
end
