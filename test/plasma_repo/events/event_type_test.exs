defmodule PlasmaRepo.Events.EventTypesTest do
  use PlasmaRepo.DataCase
  alias PlasmaRepo.Events.EventType
  import PlasmaRepo.Factory

  def clear_cache(_context) do
    PlasmaRepo.Events.EventTypeCache.flush()
    :ok
  end

  setup :clear_cache

  @type_name "type.name"
  describe "EventType" do
    test "create_event_type/1 succeed with valid name" do
      ret = EventType.create_event_type(@type_name)
      assert {:ok, event_type} = ret
      assert %{name: @type_name} = event_type
    end

    test "create_event_type/1 fails with invalid (empty) name" do
      {:error, changeset} = EventType.create_event_type("")
      assert "required" in errors_on(changeset).name
    end

    test "create_event_type/1 fails when event type already exists with same name" do
      insert(:event_type)
      {:error, changeset} = EventType.create_event_type("event.type.test")
      assert "non_unique" in errors_on(changeset).name
    end

    test "get_or_create/1 succeed in getting existing event_type from database" do
      insert(:event_type)
      ret = EventType.get_or_create("event.type.test")
      assert {:ok, _} = ret
    end

    test "get_or_create/1 succeed in getting existing event_type from cache" do
      with {:ok, _} <- EventType.create_event_type(@type_name) do
        ret = EventType.get_or_create(@type_name)
        assert {:ok, _} = ret
      end
    end
  end
end
