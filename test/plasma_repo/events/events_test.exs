defmodule PlasmaRepo.Events.EventsTest do
  use PlasmaRepo.DataCase
  alias PlasmaRepo.Events.Events
  alias PlasmaRepo.Channels.Identifier

  def clear_cache(_context) do
    PlasmaRepo.Events.EventTypeCache.flush()
    :ok
  end

  setup :clear_cache

  describe "Events" do
    test "add_all_events/a succeed creating a single event" do
      {:ok, channel} =
        PlasmaRepo.Channels.create_channel(%{channel_id: to_string(Identifier.generate(:room))})

      assert channel.next_event_sequence == 1

      assert {:ok, channel, events} =
               Events.add_all_events(channel, [
                 %{
                   state_key: "state",
                   sender: "sender",
                   type_name: "some_type"
                 }
               ])

      assert channel.next_event_sequence == 2
      assert length(events) == 1
      [ev | _tail] = events
      assert ev.event_id == channel.last_event_id
      assert ev.channel_id == channel.id
      assert ev.sequence_number == 1
    end

    test "add_all_events/a succeed creating multiple events" do
      {:ok, channel} =
        PlasmaRepo.Channels.create_channel(%{channel_id: to_string(Identifier.generate(:room))})

      assert channel.next_event_sequence == 1

      assert {:ok, channel, events} =
               Events.add_all_events(channel, [
                 %{
                   state_key: "state",
                   sender: "sender",
                   type_name: "some_type"
                 },
                 %{
                   state_key: "state2",
                   sender: "sender2",
                   type_name: "some_other_type"
                 },
                 %{
                   state_key: "state",
                   sender: "sender2",
                   type_name: "some_type"
                 }
               ])

      assert channel.next_event_sequence == 4
      assert length(events) == 3
    end
  end
end
