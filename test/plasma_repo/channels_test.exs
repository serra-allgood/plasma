defmodule Plasma.ChannelsTest do
  use PlasmaRepo.DataCase
  alias PlasmaRepo.Channels
  alias PlasmaRepo.Channels.Identifier

  def clear_cache(_context) do
    PlasmaRepo.Events.EventTypeCache.flush()
    :ok
  end

  setup :clear_cache

  describe "Channels" do
    test "exists? should return true when channel exists" do
      channel_id = to_string(Identifier.generate(:room))
      assert {:ok, _} = Channels.create_channel(%{channel_id: channel_id})
      assert Channels.exists?(channel_id) == true
    end

    test "exists? should return false when channel doesn't exist" do
      assert {:ok, _} =
               Channels.create_channel(%{channel_id: to_string(Identifier.generate(:room))})

      assert Channels.exists?("") == false
    end

    test "create_channel/1 should succeed creating a random room" do
      assert {:ok, _} =
               Channels.create_channel(%{channel_id: to_string(Identifier.generate(:room))})
    end
  end
end
