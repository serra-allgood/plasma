defmodule PlasmaHS.RoomServer.RoomState do
  defstruct [:room_id, :mx_room_id, :create_event, :creator, :power_levels, :join_rule]
end