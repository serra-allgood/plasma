defmodule PlasmaRepo.MatrixRooms.CreateRoomRequest do
  require Logger
  use Ecto.Schema

  @visibilities ["public", "private"]
  @presets ["private_chat", "public_chat", "trusted_private_chat"]

  embedded_schema do
    field :visibility, :string
    field :room_version, :string
    field :creation_content, :map
    field :power_level_content_override, :map
    field :preset, :string
    field :name, :string
    field :topic, :string
    embeds_many :initial_state, StateEvent do
      field :type, :string
      field :state_key, :string, default: ""
      field :content, :string
    end
  end

  def cast(request, params \\ %{}) do
    request
    |> Ecto.Changeset.cast(params, [
      :visibility,
      :room_version,
      :creation_content,
      :power_level_content_override,
      :name,
      :topic
    ])
    |> Ecto.Changeset.cast_embed(:initial_state, with: &cast_state_event/2)
    |> Ecto.Changeset.validate_inclusion(:visibility, @visibilities, message: "bad_type")
    |> Ecto.Changeset.validate_inclusion(:preset, @presets, message: "bad_type")
  end

  defp cast_state_event(schema, params) do
    schema
    |> Ecto.Changeset.cast(params, [:type, :state_key, :content])
    |> Ecto.Changeset.validate_required([:type, :state_key])
  end
end