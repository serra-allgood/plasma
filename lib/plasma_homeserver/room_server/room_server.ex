defmodule PlasmaHS.RoomServer do
  use GenServer

  alias PlasmaRepo.Rooms
  alias PlasmaHS.RoomServer.RoomState


#  def create_room(request) do
#      request_changeset = Ecto.Changeset.change(request)
#
#      start_room = :room
#                   |> PlasmaRepo.Channels.Identifier.generate
#                   |> PlasmaRepo.MatrixRooms.start_room_channel

#      with {:ok, request} <- Ecto.Changeset.apply_action(request_changeset, :insert),
#           {:ok, room_channel} <- start_room,
#           {:ok, _events, _state} <- GenServer.call(room_channel, %PlasmaRepo.MatrixRooms.Commands.CreateRoom {
#             sender: sender,
#             visibility: request.visibility,
#             room_version: request.room_version,
#             creation_content: request.creation_content,
#             power_level_content_override: request.power_level_content_override
#           }),
#           {:ok, _, _state} <- change_preset(room_channel, sender, request.preset, request.visibility) do
#        add_initial_state(room_channel, sender, request.initial_state)
#        if not is_nil(request.name) do
#          add_state_event(room_channel, sender, EventTypes.m_room_name, "", request.name)
#        end
#        if not is_nil(request.topic) do
#          add_state_event(room_channel, sender, EventTypes.m_room_topic, "", request.topic)
#        end
#        get_state(room_channel)
#      end
#  end

  defp via_tuple(room_id), do: {:via, Horde.Registry, {Plasma.PlasmaRegistry, room_id}}

  @doc """
  Try to start a room server. If the room is already started, the existing pid is returned
  """
  @spec start_room_server(String.t()) :: {:ok, pid()} | :ignore | {:error, {:already_started, pid()} | term()}
  def start_room_server(room_id) do
    with room when not is_nil(room) <- Rooms.get_room(room_id),
         {:error, {:already_started, pid}} <- GenServer.start_link(__MODULE__, [room], name: via_tuple(room.id)) do
      {:ok, pid}
    else
      nil -> {:error, "No room exists in repo with id #{room_id}"}
      {:ok, _} = pid -> pid
    end
  end

  def init([room]) do
    {:ok,
      %RoomState{
        room_id: room.id,
        mx_room_id: room.mx_room_id
      }
    }
  end

  end