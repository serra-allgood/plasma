defmodule PlasmaWeb.Plugs.AuthSessionCache do
  use Nebulex.Cache,
    otp_app: :plasma,
    adapter: Nebulex.Adapters.Dist

  defmodule Primary do
    use Nebulex.Cache,
      otp_app: :plasma,
      adapter: Nebulex.Adapters.Local
  end
end
