defmodule PlasmaWeb.Plugs.RequireAccessToken do
  alias Plug.Conn
  import PlasmaWeb.Errors

  @moduledoc """
  RequireAccessToken checks if a token (previously extracted by ExtractAccessToken plug) is present in `conn` assigns.
  If yes, the token presence in checked in known token table. Then `conn.assign` is completed with :
  - user_id which the token belongs to
  - device_id associated with the token
  Else, the connection is halted
  """

  def init(default), do: default

  def call(conn, _default) do
    case Map.fetch(conn.assigns, :access_token) do
      {:ok, token} ->
        access_token = PlasmaRepo.Users.AccessTokens.decode_token(token)
        if(access_token != nil) do
          Conn.assign(conn, :user_id, access_token.user_id) |> Conn.assign(:device_id, access_token.device_id)
        else
          conn |> json_error(:m_unknown_token) |> Conn.halt()
        end
      _ ->
        # Token is not found in conn assign then halt connection
        conn |> json_error(:m_missing_token) |> Conn.halt()
    end
  end
end