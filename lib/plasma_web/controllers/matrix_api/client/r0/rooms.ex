defmodule PlasmaWeb.Controllers.MatrixApi.Client.R0.Room do
  use PlasmaWeb, :controller
  use PlasmaWeb.Controllers.MatrixApi.MatrixController
  alias PlasmaRepo.MatrixRooms.CreateRoomRequest
  import PlasmaWeb.Errors
  require Logger

  def create_room(conn, params) do
    room_changeset =
      CreateRoomRequest.cast(
        %CreateRoomRequest{
          visibility: "private",
          room_version: 1,
          creation_content: %{},
          power_level_content_override: %{},
          initial_state: %{}
        },
        params
      )
      |> Ecto.Changeset.apply_action(:insert)

    case room_changeset do
      {:error, errors} ->
        send_changeset_error_to_json(conn, errors)
      {:ok, request} ->
        sender = conn.assigns.user_id
        ret = PlasmaRepo.MatrixRooms.create_room(sender, request)
        conn |> json(%{"room_id" => ret.room_id})
    end
  end

  def get_keys(conn,_) do
    conn |> json_error(:p_not_implemented)
  end
end