defmodule PlasmaWeb.Controllers.MatrixApi.Client.R0.PresenceController do
  use PlasmaWeb, :controller
  use PlasmaWeb.Controllers.MatrixApi.MatrixController
  require Logger

  def put_presence_status(conn, %{"userId" => userId, "presence" => presence}) do
    #TODO: publish presence info
    Logger.debug("'#{userId}' presence set to #{presence}")
    conn |> json(%{})
    #TODO: According to https://matrix.to/#/!YkZelGRiqijtzXZODa:matrix.org/$1558642639163TnUoJ:t2l.io?via=matrix.org&via=matrix.allmende.io&via=dodsorf.as
    #a M_FORBIDDEN error should be sent if userId don't match auth'ed user_id
  end
end