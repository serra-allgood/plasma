defmodule PlasmaWeb.Controllers.MatrixApi.Client.R0.CapabilitiesController do
  use PlasmaWeb, :controller
  use PlasmaWeb.Controllers.MatrixApi.MatrixController
  require Logger

  def get_capabilities(conn, _params) do
    Logger.warn("Get capabilities response is hardcoded")
    conn |> json(%{
      "capabilities" => %{
        "m.change.password" => %{
          "enabled" => false
        },
        "m.room_versions" => %{
          "default" => "1",
          "available"=> %{
            "1" => "unstable",
            "2" => "unstable",
            "3" => "unstable",
            "4" => "unstable",
            "5" => "unstable"
            }
        }
      }
    })
  end

end