defmodule PlasmaRepo.Repo do
  use Ecto.Repo,
    otp_app: :plasma,
    adapter: Ecto.Adapters.Postgres
end
