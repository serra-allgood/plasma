defmodule PlasmaRepo.Events.Events do
  require Logger
  alias PlasmaRepo.Repo
  alias PlasmaRepo.Channels.Channel
  alias PlasmaRepo.Events.Event

  @doc """
  Add a list of event to a channel.

  Create events rows, generate event ids, manages prev_event, sequences and update channel
  """
  @spec add_all_events(Channel.t(), [map]) :: {:ok, Channel.t(), [Event.t()]} | {:error, atom}
  def add_all_events(channel, events) do
    with channel <- PlasmaRepo.Channels.get_channel(channel.channel_id),
         {:ok, changes, next_sequence} <-
           prepare_add_events_changes(channel, events) do
      transac_result =
        Enum.reduce(changes, Ecto.Multi.new(), fn e, multi ->
          Ecto.Multi.insert(multi, e.changes.sequence_number, e)
        end)
        |> Ecto.Multi.update(
             :updated_channel,
             Channel.update_last_sequence(channel, next_sequence)
           )
        |> Repo.transaction()

      case transac_result do
        {:ok, results} ->
          {:ok, Map.get(results, :updated_channel),
            Map.drop(results, [:updated_channel]) |> Map.values()}

        _ = err ->
          Logger.error("Failed to create events in channel #{channel.id} : #{inspect(err)}")
          {:error, :transaction_failed, err}
      end
    else
      nil -> {:error, :channel_unknown}
      {:error, :invalid_event_data, events} -> {:error, :invalid_data, events}
    end
  end

  @doc """
  Create a list a changeset for appending event maps to a channel.

  The Event list is prepared so events ar linked :
  - the first event.prev_event_id is set to the channel last_event_id
  - each successive event prev_even_id is set to its predecessor event_id

  Returns:
  - List of events changeset
  - Last event ID
  - next channel sequence number
  """
  @spec prepare_add_events_changes(Channel.t(), [Event.t()]) ::
          {[Change.t()], [String.t()], integer}
  def prepare_add_events_changes(channel, events) do
    changes =
      Enum.map(events, fn event ->
        Event.create_changeset(%Event{}, Map.put(event, :channel_id, channel.id))
      end)

    if Enum.any?(changes, fn change -> change.valid? == false end) do
      Logger.warn("One or more events contains invalid data: #{inspect(changes)}")
      {:error, :invalid_event_data, Enum.find(changes, fn change -> change.valid? == false end)}
    else
      # Build a list of sequences to zip with new event changesets
      last_sequence = channel.next_event_sequence + length(changes)
      sequences = channel.next_event_sequence..last_sequence

      # Do Zip
      changes =
        Enum.zip([changes, sequences])
        |> Enum.map(fn {change, seq} -> Event.add_event_sequence(change, seq) end)

      {:ok, changes, last_sequence}
    end
  end

end