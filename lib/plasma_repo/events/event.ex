defmodule PlasmaRepo.Events.Event do
  use PlasmaRepo.Schema

  alias PlasmaRepo.Events.{Event, EventContent}
  alias PlasmaRepo.Events.EventType
  alias PlasmaRepo.Events.Identifier
  alias PlasmaRepo.Repo

  @type t :: %__MODULE__{
          event_id: String.t(),
          sequence_number: integer,
          state_key: String.t(),
          sender: String.t(),
          prev_event_id: String.t(),
          type_name: String.t(),
          content: any
        }

  schema "events" do
    field :event_id, :string
    field :sequence_number, :integer
    field :state_key, :string, default: nil
    field :sender, :string
    field :prev_event_id, :string
    belongs_to :channel, Channel
    belongs_to :event_type, EventType
    has_one :content, EventContent
    field :type_name, :string, virtual: true
    timestamps()
  end

  def create_event(attrs) do
    %Event{}
    |> create_changeset(attrs)
    |> Repo.insert()
  end

  def create_changeset(event, attrs \\ %{}) do
    event
    |> cast(attrs, [
      :sequence_number,
      :channel_id,
      :state_key,
      :sender,
      :prev_event_id,
      :event_type_id,
      :type_name
    ])
    |> fetch_event_type()
    |> validate_required([:channel_id, :sender, :event_type], message: "required")
    |> unique_constraint(:event_id, message: "unique")
    |> foreign_key_constraint(:event_type_id)
    |> put_assoc(
      :content,
         EventContent.create_changeset(%EventContent{}, attrs)
    )
  end

  def add_prev_id(changeset, prev_id) do
    case changeset do
      %Ecto.Changeset{valid?: true} -> change(changeset, prev_event_id: prev_id)
      _ -> changeset
    end
  end

  def add_event_sequence(changeset, sequence) do
    case changeset do
      %Ecto.Changeset{valid?: true} -> change(changeset, sequence_number: sequence)
      _ -> changeset
    end
  end

  defp fetch_event_type(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{type_name: type_name}} ->
        {:ok, event_type} = EventType.get_or_create(type_name)
        put_assoc(changeset, :event_type, event_type)

      _ ->
        changeset
    end
  end

  defp generate_event_id(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true} ->
        change(changeset, event_id: to_string(Identifier.generate(:event)))
      _ ->
        changeset
    end
  end
end
