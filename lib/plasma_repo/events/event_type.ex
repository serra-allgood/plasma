defmodule PlasmaRepo.Events.EventType do
  require Logger
  use PlasmaRepo.Schema
  alias PlasmaRepo.Events.EventType
  alias PlasmaRepo.Events.EventTypeCachableRepo

  @type t :: %__MODULE__{
               name: String.t(),
               inserted_at: DateTime.t()
             }

  schema "event_types" do
    field :name, :string
    timestamps(updated_at: false)
  end

  defp create_changeset(event_type, attrs) do
    event_type
    |> cast(attrs, [:name])
    |> validate_required([:name], message: "required")
    |> unique_constraint(:name, message: "non_unique")
  end

  @spec get_or_create(String.t()) :: {:ok, EventType.t()} | {:error, term()}
  def get_or_create(type_name) do
    case get_by_name(type_name) do
      nil ->
        create_event_type(type_name)

      {:error, cause} ->
        Logger.warn("EventTypeCache get error: #{cause}")
        {:error, cause}

      event_type ->
        {:ok, event_type}
    end
  end

  def get_by_name(type_name) do
    EventTypeCachableRepo.get_by(EventType, name: type_name)
  end

  def get_by_id(id) do
    EventTypeCachableRepo.get(EventType, id)
  end

  def create_event_type(type_name) do
    EventTypeCachableRepo.insert(create_changeset(%EventType{}, %{name: type_name}))
  end
end
