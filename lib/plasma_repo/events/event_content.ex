defmodule PlasmaRepo.Events.EventContent do
  use PlasmaRepo.Schema

  alias PlasmaRepo.Events.Event

  schema "event_contents" do
    field :content, :any, virtual: true
    field :content_type, :string, default: "string"
    field :string_content, :string
    field :json_content, :map
    field :compressed_binary_content, :binary
    belongs_to :event, Event
    timestamps()
  end

  def create_changeset(event_content, attrs \\ %{}) do
    event_content
    |> cast(attrs, [:content, :content_type])
    |> validate_required([:content_type], message: "required")
    |> validate_inclusion(:content_type, ["string", "json", "binary"])
    |> set_content()
  end

  defp set_content(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{content_type: content_type, content: content}} ->
        case content_type do
          "string" -> change(changeset, string_content: content)
          "json" -> change(changeset, json_content: content)
          "binary" -> change(changeset, binary_content: compress(content))
        end

      _ ->
        changeset
    end
  end

  defp compress(data) do
    :zlib.compress(data)
  end
end
