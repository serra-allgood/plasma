defmodule PlasmaRepo.Events.EventTypeCache do
  use Nebulex.Cache,
    otp_app: :plasma,
    adapter: Nebulex.Adapters.Local
end

defmodule PlasmaRepo.Events.EventTypeCachableRepo do
  use NebulexEcto.Repo,
    cache: PlasmaRepo.Events.EventTypeCache,
    repo: PlasmaRepo.Repo
end
