defmodule PlasmaRepo.Users do
  require Logger
  require Ecto.Query
  alias PlasmaRepo.Users.Filter
  alias PlasmaRepo.Users.{User, Device, AccessToken}
  alias PlasmaRepo.Repo

  @doc """
  Delete a device and associated token
  """
  @spec delete_device(String.t()) :: {:ok, Device.t()} | {:error, Changeset.t()}
  def delete_device(device_id) do
    Repo.get(Device, device_id) |> Repo.delete
  end

  @doc """
  Delete all devices known for a user
  """
  @spec delete_user_devices(String.t()) :: UserState.t()
  def delete_user_devices(user_id) do
    query = Ecto.Query.from d in Device,
                            join: u in User, on: u.id == d.user_id,
                            join: a in AccessToken, on: d.id == a.device_id,
                            where: u.id == ^user_id,
                            select: d
    Repo.delete_all(query)
  end

  @doc """
  Add device to an existing user
  """
  def add_device(device_attrs) do
    Device.create_changeset(%Device{}, device_attrs) |> Repo.insert
  end

  @doc """
  Get user and its devices from repo with given matrix user_id
  """
  @spec get_by_mx_user_id_with_devices(String.t()) :: User.t() | nil
  def get_by_mx_user_id_with_devices(mx_user_id) do
    Repo.get_by(User, mx_user_id: mx_user_id) |> Repo.preload([:devices])
  end

  @doc """
  Get user and its devices from repo
  """
  @spec get_with_devices(String.t()) :: User.t() | nil
  def get_with_devices(user_id) do
    Repo.get(User, user_id) |> Repo.preload([:devices])
  end

  @spec register_user(map()) :: {:ok}
  def register_user(request) do
    with device <- Device.create_changeset(%Device{}, request),
         user <- User.register_changeset(%User{devices: [device]}, request),
         #user_device_assoc <- User.register_changeset(%User{}, request),
         {:ok, res} <- Repo.insert(user) do
      {:ok, res}
    end
  end

  @doc """
  get user from repo given maxtrix user_id and check password
  """
  @spec get_user_check_password(String.t(), String.t()) :: {:ok, User.t()} | {:error, :login_failed}
  def get_user_check_password(mx_user_id, password) do
    with user when not is_nil(user) <- PlasmaRepo.Users.get_by_mx_user_id_with_devices(mx_user_id),
         password_hash when not is_nil(password_hash)<- user.password_hash do
      case Argon2.verify_pass(password, password_hash) do
        true -> {:ok, user}
        false -> {:error, :login_failed}
      end
    else
      _ ->
        Argon2.no_user_verify()
        {:error, :login_failed}
    end
  end

  @doc """
  Creates a new query filter for the given user ID
  """
  @spec create_filter(String.t(), map()) :: {:ok, Filter.t()} | {:error, Ecto.Changeset.t()}
  def create_filter(user_id, query) do
    filter_id = Plasma.Utils.Randomizer.unique_id()
    %Filter{}
    |> Filter.create_changeset(%{"filter_id" => filter_id, "user_id" => user_id, "query" => query})
    |> Repo.insert()
  end

  @spec get_filter(String.t(), String.t()) :: Filter.t() | nil
  def get_filter(user_id, filter_id) do
    Repo.get_by(Filter, [user_id: user_id, filter_id: filter_id])
  end
end
