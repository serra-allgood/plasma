defmodule PlasmaRepo.Users.Device do
  require Logger
  use PlasmaRepo.Schema

  @type t :: %__MODULE__{
               mx_device_id: String.t(),
               initial_device_display_name: String.t(),
               user: User.t(),
               user_id: String.t(),
               access_token: AccessToken.t()
             }

  schema "devices" do
    field :mx_device_id, :string
    field :initial_device_display_name, :string
    belongs_to :user, PlasmaRepo.Users.User
    has_one :access_token, PlasmaRepo.Users.AccessToken
    timestamps()
  end

  def changeset(device, attrs \\ %{}) do
    device
    |> cast(attrs, [:user_id, :mx_device_id, :initial_device_display_name])
    |> validate_required([:mx_device_id])
    |> assoc_constraint(:user)
    |> unique_constraint(:mx_device_id, message: "device_already_exists")
  end

  def create_changeset(device, attrs \\ %{}) do
    device
    |> changeset(attrs)
  end
end
