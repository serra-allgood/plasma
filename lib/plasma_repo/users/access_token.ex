defmodule PlasmaRepo.Users.AccessToken do
  use PlasmaRepo.Schema

  @type t :: %__MODULE__{
               id: binary,
               expires_at: DateTime.t(),
               user_id: String,
               user: User.t(),
               device_id: String,
               device: Device.t(),
               inserted_at: DateTime.t(),
               updated_at: DateTime.t()
             }

  schema "access_tokens" do
    field :expires_at, :utc_datetime, default: nil
    belongs_to :user, PlasmaRepo.Users.User
    belongs_to :device, PlasmaRepo.Users.Device
    timestamps()
  end

  def create_changeset(access_token, attrs \\ %{}) do
    access_token
    |> cast(attrs, [:device_id, :user_id, :expires_at])
    |> unique_constraint(:device, name: :access_tokens_device_id, message: "unique")
    |> assoc_constraint(:device)
    |> assoc_constraint(:user)

  end
end
