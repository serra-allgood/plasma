defmodule PlasmaRepo.Users.User do
  require Logger
  use PlasmaRepo.Schema

  @type t :: %__MODULE__{
               mx_user_id: String.t(),
               password: String.t(),
               password_hash: String.t(),
               kind: String.t(),
               devices: [Device.t()],
               inserted_at: DateTime.t(),
               updated_at: DateTime.t()
             }

  schema "users" do
    field :mx_user_id, :string
    field :password, :string, virtual: true
    field :password_hash, :string
    field :kind, :string
    has_many :devices, PlasmaRepo.Users.Device
    timestamps()
  end

  def changeset(user, attrs \\ %{}) do
    user
    |> cast(attrs, [:mx_user_id, :password, :password_hash, :kind])
    |> unique_constraint(:mx_user_id, message: "unique")
    |> validate_username()
    |> validate_password()
      #|> validate_required([:user_id, :password], message: "empty_user_id_or_password")
    |> unique_constraint(:mx_user_id, message: "user_in_use")
  end

  def register_changeset(user, attrs \\ %{}) do
      user
      |> changeset(attrs)
      |> cast_assoc(:devices)
    end

  # Validate registration username
  # - if no username is provided, generate a new random one
  # - if a localpart is given, build full username and check availability
  defp validate_username(%Ecto.Changeset{valid?: true} = changeset) do
    case changeset do
      %Ecto.Changeset{changes: %{mx_user_id: mx_user_id}} ->
        if PlasmaRepo.Channels.Identifier.valid?(mx_user_id, :user) do
          changeset
        else
          new_user = PlasmaRepo.Channels.Identifier.new(:user, mx_user_id)
          if PlasmaRepo.Channels.Identifier.valid?(new_user) do
            Ecto.Changeset.put_change(changeset, :mx_user_id, to_string(new_user))
          else
            Ecto.Changeset.add_error(changeset, :mx_user_id, "invalid_user_id")
          end
        end
      _ -> Ecto.Changeset.put_change(changeset, :mx_user_id, to_string(PlasmaRepo.Channels.Identifier.generate(:user)))
    end
  end
  defp validate_username(changeset), do: changeset

  defp validate_password(%Ecto.Changeset{valid?: true, changes: %{password: password}} = changeset) do
    change(changeset, Argon2.add_hash(password))
  end
  defp validate_password(changeset), do: changeset
end