defmodule PlasmaRepo.Channels do
  require Logger
  import Ecto.Query
  alias PlasmaRepo.Repo
  alias PlasmaRepo.Channels.Channel
  alias PlasmaRepo.Channels.Event

  def create_channel(attrs) do
    %Channel{}
    |> Channel.create_changeset(attrs)
    |> Repo.insert()
  end

  def get_channel(channel_id) do
    Repo.get_by(Channel, channel_id: channel_id)
  end

  def exists?(channel_id) do
    Repo.exists?(from c in Channel, where: c.channel_id == ^channel_id)
  end

end
