defmodule PlasmaRepo.Channels.Channel do
  use PlasmaRepo.Schema

  @type t :: %__MODULE__{
          channel_id: String.t(),
          next_event_sequence: integer,
          last_event_id: integer
        }

  schema "channels" do
    field :channel_id, :string
    field :next_event_sequence, :integer
    field :last_event_id, :string
  end

  def create_changeset(channel, attrs \\ %{}) do
    channel
    |> cast(attrs, [:channel_id, :next_event_sequence, :last_event_id])
    |> put_change(:next_event_sequence, 1)
    |> validate_required([:channel_id, :next_event_sequence], message: "required")
    |> unique_constraint(:channel_id, message: "non_unique")
  end

  def update_last_sequence(channel, next_sequence) do
    change(channel, next_event_sequence: next_sequence)
  end
end
