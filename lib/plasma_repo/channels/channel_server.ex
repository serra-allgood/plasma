defmodule PlasmaRepo.CommandHandler do
  @callback init(channel_id :: String.t()) :: term
  @callback handle_command(command :: struct, state :: term) :: {:ok, [struct]} | {:error, atom}
  @callback apply_event(Event.t(), state :: term) :: term
  @callback cast_side_effects(Event.t(), state :: term) :: none

  # defmacro
  @doc false
  defmacro __using__(_) do
    quote do
    end
  end
end

defmodule PlasmaRepo.Channels.ChannelState do
  defstruct [:channel, :command_handler, :handler_state]
end

defmodule PlasmaRepo.Channels.ChannelServer do
  use GenServer
  require Logger

  # Public API

  def child_spec(opts) do
    channel_id = Keyword.get(opts, :channel_id, __MODULE__)

    %{
      id: "#{__MODULE__}_#{channel_id}",
      start: {__MODULE__, :start_link, [channel_id]},
      shutdown: 10_000,
      restart: :permanent
    }
  end

  def init([channel_id, command_handler]) do
    case PlasmaRepo.Channels.get_channel(channel_id) do
      nil ->
        Logger.debug("Channel #{channel_id} doesn't exist yes, creating it.")

        with {:ok, channel} <- PlasmaRepo.Channels.create_channel(%{channel_id: channel_id}) do
          {:ok,
           %PlasmaRepo.Channels.ChannelState{
             channel: channel,
             command_handler: command_handler,
             handler_state: command_handler.init(channel_id)
           }}
        else
          error ->
            Logger.error("Failed to initialize channel : #{inspect(error)}")
            {:error, :initialization_failed}
        end

      channel ->
        Logger.debug("Channel #{channel_id} already exists, loading state.")

        {:ok,
         %PlasmaRepo.Channels.ChannelState{
           channel: channel,
           command_handler: command_handler,
           handler_state: %{}
         }}
    end
  end

  def start_link(channel_id, command_handler) do
    GenServer.start_link(__MODULE__, [channel_id, command_handler], name: via_tuple(channel_id))
  end

  @spec start_channel(String.t(), term) :: {:ok, pid()} | :ignore | {:error, {:already_started, pid()} | term()}
  def start_channel(channel_id, handler) do
    case PlasmaRepo.Channels.ChannelServer.start_link(channel_id, handler) do
      {:error, {:already_started, pid}} -> {:ok, pid}
      {:ok, pid} -> {:ok, pid}
    end
  end

  # Internal API

  defp via_tuple(channel_id), do: {:via, Horde.Registry, {Plasma.PlasmaRegistry, channel_id}}

  def recover(_channel_id) do
    # TODO : Implement recover from snapshot + events
  end

  def handle_call(:get_state, _from, channel_state) do
    {:reply, channel_state.handler_state, channel_state}
  end

  def handle_call(command, _from, channel_state) do
    Logger.debug("Receiving command #{inspect(command)}")
    # Process command with current channel state
    command_handler = channel_state.command_handler

    with {:ok, command_events} <-
           command_handler.handle_command(command, channel_state.handler_state),
         {:ok, channel, events} <- PlasmaRepo.Channels.Events.add_all_events(channel_state.channel, command_events) do
      # Apply events to state
      applied_state =
        Enum.reduce(events, channel_state.handler_state, fn event, acc_state ->
          command_handler.apply_event(event, acc_state)
        end)

      # Cast side effects
      spawn_side_effects(events, command_handler, applied_state)

      {:reply, {:ok, events, applied_state},
       %{channel_state | channel: channel, handler_state: applied_state}}
    else
      {:error, reason, details} -> {:reply, {:error, reason, details}, channel_state}
    end
  end

  defp spawn_side_effects(events, handler, handler_state) do
    Enum.map(
      events,
      fn event ->
        Logger.debug("Spawning side effects of event #{event.id}")

        if Mix.env() == :test do
          handler.cast_side_effects(event, handler_state)
        else
          Task.Supervisor.start_child(
            PlasmaRepo.TaskSupervisor,
            fn -> handler.cast_side_effects(event, handler_state) end
          )
        end
      end
    )
  end
end
