defmodule PlasmaRepo.Channels.Snapshot do
  use PlasmaRepo.Schema

  alias PlasmaRepo.Channels.Channel

  schema "snaphots" do
    field :snapshot_state, :map
    field :event_sequence_number, :integer
    belongs_to :channel, Channel
    timestamps(updated_at: false)
  end

  # def get_latest_snapshot_for_channel(channel_id) do
  #  query = from s in Snapshot,
  #    join: c in Channel, where: s.channel_id == c.id
  # end
end
