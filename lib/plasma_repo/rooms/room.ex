defmodule PlasmaRepo.Rooms.Room do
  use PlasmaRepo.Schema

  alias PlasmaRepo.Events.{Event, EventContent}
  alias PlasmaRepo.Events.EventType
  alias PlasmaRepo.Events.Identifier
  alias PlasmaRepo.Repo

  @type t :: %__MODULE__{
               mx_room_id: String.t(),
               is_public: boolean
             }

  schema "rooms" do
    field :mx_room_id, :string
    field :is_public, :boolean, default: false
    timestamps()
  end

  def changeset(room, attrs \\ %{}) do
    room
    |> cast(attrs, [:mx_room_id, :visibility])
    |> validate_required([:mx_room_id])
    |> unique_constraint(:mx_room_id, message: "unique")
  end

  def create_changeset(room, attrs \\ %{}) do
    room
    |> changeset(attrs)
  end
end