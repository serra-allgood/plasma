defmodule PlasmaRepo.Rooms do
  alias PlasmaRepo.Rooms.Room
  alias PlasmaRepo.Repo

  def create_room(params) do
    Room.create_changeset(%Room{}, params) |> Repo.insert
  end

  @doc """
  Get a room from the repo
  """
  @spec get_room(String.t) :: Room | nil
  def get_room(room_id) do
    Repo.get(Room, room_id)
  end
end