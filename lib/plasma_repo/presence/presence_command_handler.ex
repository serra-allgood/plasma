defmodule PlasmaRepo.Presence.CommandHandler do
  use PlasmaRepo.CommandHandler
  alias PlasmaRepo.Channels.Event
  require Logger

  defmodule PresenceState do
    defstruct [:channel_id]
  end

  alias PlasmaRepo.Presence.CommandHandler.PresenceState

  def init(channel_id) do
    %PresenceState{
      channel_id: channel_id
    }
  end

  def handle() do

  end
end