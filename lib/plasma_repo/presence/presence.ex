defmodule PlasmaRepo.Presence do
  require Logger

  @presence_channel_id PlasmaRepo.Channels.Identifier.new(:user, "__PRESENCE__")


  @spec get_presence_channel() :: {:ok, pid()} | {:error}
  def get_presence_channel() do
    PlasmaRepo.Channels.ChannelServer.start_channel(@presence_channel_id, PlasmaRepo.Presence.CommandHandler)
  end

end