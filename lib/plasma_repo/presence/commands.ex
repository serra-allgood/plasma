defmodule PlasmaRepo.Presence.Commands do
  defmodule SetPresence do
    @enforce_keys [:user_id, :device_id, :access_token]
    defstruct [:user_id, :presence, :status_msg, :last_active_]
  end
end