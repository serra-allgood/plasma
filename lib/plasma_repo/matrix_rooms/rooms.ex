defmodule PlasmaRepo.MatrixRooms do
  require Logger
  require PlasmaRepo.MatrixRooms.EventTypes
  alias PlasmaRepo.MatrixRooms.CreateRoomRequest
  alias PlasmaRepo.MatrixRooms.EventTypes

  @spec get_room_channel(PlasmaRepo.Channels.Identifier.room()) :: {:ok, pid()} | {:error, :no_user}
  def get_room_channel(%PlasmaRepo.Channels.Identifier{} = identifier) do
    PlasmaRepo.MatrixRooms.get_room_channel(to_string(identifier))
  end

  @spec get_room_channel(String.t()) :: {:ok, pid()} | {:error, :no_room}
  def get_room_channel(channel_id) do
    if PlasmaRepo.Channels.exists?(channel_id) do
      start_room_channel(channel_id)
    else
      {:error, :no_room}
    end
  end

  @spec start_room_channel(PlasmaRepo.Channels.Identifier.room()) :: {:ok, pid()} | {:error, :no_room}
  def start_room_channel(%PlasmaRepo.Channels.Identifier{} = identifier) do
    PlasmaRepo.MatrixRooms.start_room_channel(to_string(identifier))
  end

  @spec start_room_channel(String.t()) :: {:ok, pid()} | :ignore | {:error, {:already_started, pid()} | term()}
  def start_room_channel(channel_id) do
    PlasmaRepo.Channels.ChannelServer.start_channel(channel_id, PlasmaRepo.MatrixRooms.CommandHandler)
  end

  @spec get_state(pid()) :: RoomState.t()
  def get_state(pid) do
    GenServer.call(pid, :get_state)
  end

  def create_room(sender, %CreateRoomRequest{} = request) do
    request_changeset = Ecto.Changeset.change(request)

    start_room = :room
    |> PlasmaRepo.Channels.Identifier.generate
    |> PlasmaRepo.MatrixRooms.start_room_channel

    with {:ok, request} <- Ecto.Changeset.apply_action(request_changeset, :insert),
         {:ok, room_channel} <- start_room,
         {:ok, _events, _state} <- GenServer.call(room_channel, %PlasmaRepo.MatrixRooms.Commands.CreateRoom {
           sender: sender,
           visibility: request.visibility,
           room_version: request.room_version,
           creation_content: request.creation_content,
           power_level_content_override: request.power_level_content_override
         }),
         {:ok, _, _state} <- change_preset(room_channel, sender, request.preset, request.visibility) do
      add_initial_state(room_channel, sender, request.initial_state)
      if not is_nil(request.name) do
        add_state_event(room_channel, sender, EventTypes.m_room_name, "", request.name)
      end
      if not is_nil(request.topic) do
        add_state_event(room_channel, sender, EventTypes.m_room_topic, "", request.topic)
      end
      get_state(room_channel)
    end
  end

  defp change_preset(channel, sender, preset, visibility) do
    join_rule = case preset do
      "private_chat" -> "invite"
      "trusted_private_chat" -> "invite"
      "public_chat" -> "public"
      nil -> case visibility do
          "public" -> "public"
               "private" -> "invite"
        end
    end
    GenServer.call(channel, %PlasmaRepo.MatrixRooms.Commands.ChangeJoinRules{sender: sender, join_rule: join_rule})
  end

  defp add_initial_state(channel, sender, initial_state) do
    initial_state
    |> Enum.reduce(nil, fn event, _acc -> add_state_event(channel, sender, event.type, event.state_key, event.content)
    end)
  end

  defp add_state_event(channel, sender, type, state_key, content) do
    GenServer.call(channel, %PlasmaRepo.MatrixRooms.Commands.AddStateEvent{
      sender: sender,
      type: type,
      state_key: state_key,
      content: content})
  end
end