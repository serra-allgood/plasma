defmodule PlasmaRepo.MatrixRooms.EventTypes do
  use Plasma.Utils.Constants

  define m_room_create, "m.room.create"
  define m_room_power_levels, "m.room.power_levels"
  define m_room_join_rules, "m.room.join_rules"
  define m_room_name, "m.room.name"
  define m_room_topic, "m.room.topic"

end