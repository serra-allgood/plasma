defmodule PlasmaRepo.MatrixRooms.CommandHandler do
  use PlasmaRepo.CommandHandler
  require Logger
  require PlasmaRepo.MatrixRooms.EventTypes
  alias PlasmaRepo.MatrixRooms.Commands.{CreateRoom, ChangeJoinRules, AddStateEvent}
  alias PlasmaRepo.Events.Event
  alias PlasmaRepo.MatrixRooms.EventTypes

  @matrix_config Application.get_env(:repo, :matrix)
  @defaults_power_levels @matrix_config[:default_power_levels]

  defmodule RoomState do
    defstruct [:room_id, :create_event, :creator, :power_levels, :join_rule]
  end

  def init(channel_id) do
    %RoomState{
      room_id: channel_id,
      create_event: nil,
      creator: nil,
      power_levels: %{},
      join_rule: nil
    }
  end

  def handle_command(%CreateRoom{} = create_command, %{create_event: nil} = state) do
    command_events = [
      %{
        sender: create_command.sender,
        type_name: EventTypes.m_room_create,
        state_key: "",
        content_type: "json",
        content: Map.merge(create_command.creation_content,%{
          creator: create_command.sender,
          room_version: create_command.room_version
        })
      },
      %{
        sender: create_command.sender,
        type_name: EventTypes.m_room_power_levels,
        state_key: "",
        content_type: "json",
        content: Map.merge(@defaults_power_levels, create_command.power_level_content_override)
                 |> Map.merge(%{users: %{create_command.sender => 100}})
      }
    ]
    case check_command_power_levels(command_events, create_command.sender, %{state | creator: create_command.sender}) do
      {:ok} -> {:ok, command_events}
      {:error, event} -> {:error, :insufficient_power_level, "#{create_command.sender} doesn't have enough power level to create event type #{event.type_name}"}
    end
  end

  def handle_command(%CreateRoom{}, _state) do
    {:error, :create_room_failed, :room_already_created}
  end

  def handle_command(%ChangeJoinRules{} = command, state) do
    command_events = [
      %{
        sender: command.sender,
        type_name: EventTypes.m_room_join_rules,
        state_key: "",
        content_type: "json",
        content: %{join_rule: command.join_rule}
      },
    ]
    case check_command_power_levels(command_events, command.sender, state) do
      {:ok} -> {:ok, command_events}
      {:error, event} -> {:error, :insufficient_power_level, "#{command.sender} doesn't have enough power level to create event type #{event.type_name}"}
    end
  end

  def handle_command(%AddStateEvent{} = command, state) do
    command_events = [
      %{
        sender: command.sender,
        type_name: command.type,
        state_key: command.state_key,
        content_type: "json",
        content: command.content
      }
    ]
    case check_command_power_levels(command_events, command.sender, state) do
      {:ok} -> {:ok, command_events}
      {:error, event} -> {:error, :insufficient_power_level, "#{command.sender} doesn't have enough power level to create event type #{event.type_name}"}
    end
  end

  #Check if a command sender has enough power levels to create all the events implied by the command sent
  # return true if user has sufficient power levels for all events
  # false otherwise (at least on event failed check)
  defp check_command_power_levels(events, user_id, state) do
    case Enum.find(events, fn event -> !PlasmaRepo.MatrixRooms.PowerLevels.user_has_sufficient_power_level(event, user_id, state) end) do
      nil -> {:ok}
      event -> Logger.debug("user #{user_id} doesn't have enough power level to create event type #{event.type_name}")
               {:error, event}

    end
  end

  def apply_event(%Event{type_name: EventTypes.m_room_create} = event, state) do
    %{state | create_event: event.event_id, creator: event.sender}
  end

  def apply_event(%Event{type_name: EventTypes.m_room_power_levels, content: content}, state) do
    %{state | power_levels: content.content}
  end

  def apply_event(%Event{type_name: EventTypes.m_room_join_rules, content: content}, state) do
    %{state | join_rule: content.content.join_rule}
  end

  def apply_event(event, state) do
    Logger.warn("Event #{event.type_name} ignored in apply_event")
    state
  end

  def cast_side_effects(event, _state) do
    Logger.debug("casting side effects for #{inspect(event)}")
  end
end
