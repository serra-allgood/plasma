defmodule PlasmaRepo.Channels.Identifier do
  # This code is based on https://github.com/bismark/matrex/blob/master/lib/matrex/identifier.ex

  alias __MODULE__, as: This

  @type user :: %This{
          type: :user,
          localpart: String.t(),
          domain: String.t()
        }
  @type room :: %This{
          type: :room,
          localpart: String.t(),
          domain: String.t()
        }
  @type event :: %This{
          type: :event,
          localpart: String.t(),
          domain: String.t()
        }
  @type room_alias :: %This{
          type: :room_alias,
          localpart: String.t(),
          domain: String.t()
        }
  @type t :: %This{
          type: atom,
          localpart: String.t(),
          domain: String.t()
        }

  defstruct [
    :type,
    :localpart,
    :domain
  ]

  @user_localpart_regex ~r|^[a-z0-9\-\.\=\_\/]+$|
  @user_sigil "@"
  @room_sigil "!"
  @event_sigil "$"
  @room_alias_sigil "#"
  @max_length 255

  def new(type, localpart, domain \\ nil) do
    domain =
      case domain do
        nil -> PlasmaWeb.Application.domain()
        _ -> domain
      end

    %This{type: type, localpart: localpart, domain: domain}
  end

  def generate(type) do
    new(type, UUID.uuid4() |> Base.hex_encode32(padding: false, case: :lower))
  end

  def valid?(%This{} = this) do
    valid_localpart?(this) && String.length(fqid(this)) < @max_length
  end

  def valid?(identifier_string) do
    case parse(identifier_string) do
      {:ok, identifier} -> valid?(identifier)
      _ -> false
    end
  end

  def valid?(identifier_string, expected) do
    case parse(identifier_string, expected) do
      {:ok, identifier} -> valid?(identifier)
      _ -> false
    end
  end

  def fqid(%This{} = this) do
    "#{sigil(this)}#{this.localpart}:#{this.domain}"
  end

  @spec parse(String.t(), atom) :: {:ok, This.t()} | :error
  def parse(id, expected) do
    with {:ok, ^expected, id} <- type_from_sigil(id),
         [localpart, domain] <- String.split(id, ":", parts: 2, trim: true) do
      {:ok, new(expected, localpart, domain)}
    else
      _ -> :error
    end
  end

  @spec parse(String.t()) :: {:ok, This.t()} | :error
  def parse(id) do
    with {:ok, type, id} <- type_from_sigil(id) do
      [localpart, domain] = String.split(id, ":", parts: 2, trim: true)
      {:ok, new(type, localpart, domain)}
    end
  end

  defp valid_localpart?(%This{type: :user, localpart: localpart}) do
    Regex.match?(@user_localpart_regex, localpart)
  end

  defp valid_localpart?(_), do: true

  @spec type_from_sigil(String.t()) :: {:ok, atom, String.t()} | :error
  defp type_from_sigil(@user_sigil <> rest), do: {:ok, :user, rest}
  defp type_from_sigil(@room_sigil <> rest), do: {:ok, :room, rest}
  defp type_from_sigil(@event_sigil <> rest), do: {:ok, :event, rest}
  defp type_from_sigil(@room_alias_sigil <> rest), do: {:ok, :room_alias, rest}
  defp type_from_sigil(_), do: :error

  defp sigil(%This{type: :user}), do: @user_sigil
  defp sigil(%This{type: :room}), do: @room_sigil
  defp sigil(%This{type: :event}), do: @event_sigil
  defp sigil(%This{type: :room_alias}), do: @room_alias_sigil
end

alias PlasmaRepo.Channels.Identifier, as: This

defimpl Jason.Encoder, for: This do
  def encode(id, options) do
    Jason.Encoder.BitString.encode(This.fqid(id), options)
  end
end

defimpl String.Chars, for: This do
  def to_string(this), do: This.fqid(this)
end
