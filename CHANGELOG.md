# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added

#### 2019-12-29
- Refactor Users, Device and access token management
- Added `GET /_matrix/client/r0/capabilities` endpoint (currently hardcoded)
- Initialize sync and pushrules controllers  

#### 2019-05-23
- API: Implement filtering endpoints    

#### Before

- Basic implementation of `Channel`, `Event` and `EventType` schema
- `Identifier` module used to generate events, room, users and room alias ID.
- ChannelServer implementation : 
   - handle commands through handlers.
   - manage events persistence.
   - manage handler state and events side effects
- API: implement `GET /_matrix/client/versions`    
- API: implement `GET /.well-known/matrix/client`    
- Implement [User-interactive authentication API](https://matrix.org/docs/spec/client_server/r0.4.0.html#user-interactive-authentication-api). Currenlty only support `m.login.dummy`.
- API: implement `POST /_matrix/client/r0/register`
- API: implement `POST /_matrix/client/r0/login`
  - currently only supports `m.login.password` login type and `m.id.user` identifier 
- API: implement `POST /_matrix/client/r0/createRoom`
    

